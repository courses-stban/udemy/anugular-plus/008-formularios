import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styles: []
})
export class DataComponent {
  forma: FormGroup;

  usuario: any = {
    nombreCompleto: {
      nombre: 'Esteban',
      apellido: 'Diaz'
    },
    email: null,
    pasatiempos: ['Correr', 'Dormir', 'Comer']
  }

  constructor() {
    console.log(this.usuario);
    this.forma = new FormGroup({
      nombreCompleto: new FormGroup(
        {
          nombre: new FormControl(
            null,
            [
              Validators.required,
              Validators.minLength(3)
            ]),
          apellido: new FormControl(
            null,
            [
              Validators.required,
              this.noDiaz
            ]),
        }
      ),
      email: new FormControl(
        null,
        [
          Validators.required,
          Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")
        ]),
      pasatiempos: new FormArray([
        new FormControl('correr', Validators.required)
      ]),
      userName: new FormControl(null, Validators.required, this.existeUsuario),
      password1: new FormControl(null, Validators.required),
      password2: new FormControl()
    });

    this.forma.get('password2').setValidators([
        Validators.required,
        this.noIgual.bind(this)
      ]);

    this.forma.get('userName').valueChanges
      .subscribe( data => console.log(data));

    this.forma.get('userName').statusChanges
      .subscribe( data => console.log(data));

    // this.forma.setValue(this.usuario);
  }

  noDiaz(control: FormControl): { [s:string]: boolean } {
    if(control.value === 'Diaz') {
      return {
        noDiaz: true
      }
    }

    return null;
  }

  noIgual(control: FormControl): { [s:string]: boolean } {
    if(control.value !== this.forma.controls['password1'].value) {
      return {
        noIguales: true
      }
    }

    return null;
  }

  existeUsuario(control: FormControl): Promise<any> | Observable<any> {
    let promesa = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          if(control.value === 'stban') {
            resolve({ existe: true })
          } else {
            resolve(null)
          }
        }, 3000)
      }
    )

    return promesa;
  }

  guardarCambios() {
    console.log(this.forma);
    console.log(this.forma.value);
    // this.forma.reset();
  }

  agregarPasatiempo() {
    (<FormArray>this.forma.controls['pasatiempos']).push(
      new FormControl(null, Validators.required)
    );
  }

}
