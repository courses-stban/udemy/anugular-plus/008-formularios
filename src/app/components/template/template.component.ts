import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styles: [`
    .ng-invalid.ng-touched:not(form) {
      border: 1px solid red;
    }
  `]
})
export class TemplateComponent {
  usuario: any = {
    nombre: null,
    apellido: null,
    correo: null,
    pais: '',
    sexo: 'Hombre',
    acepta: false
  };

  paises = [
    {
      codigo: 'CRI',
      nombre: 'Costarica'
    },
    {
      codigo: 'ESP',
      nombre: 'España'
    },
    {
      codigo: 'COL',
      nombre: 'Colombia'
    }
  ];

  sexos: string[] = ['Hombre', 'Mujer', 'Sin definir']

  constructor() { }

  guardar(forma: NgForm) {
    console.log('Submit');
    console.log('NgForm: ', forma);
    console.log('NgForm.value: ', forma.value);

    console.log('Usuario: ', this.usuario);
  }

}
